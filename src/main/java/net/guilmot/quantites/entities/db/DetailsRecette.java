package net.guilmot.quantites.entities.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "details_recette")
public class DetailsRecette {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "quantite", nullable = false)
    private double quantite;

    @OneToOne
    @JoinColumn(name = "id_unite")
    private Unite unite;

    @OneToOne
    @JoinColumn(name = "id_ingredient")
    private Ingredient ingredient;

    @OneToOne
    @JoinColumn(name = "id_recette")
    private Recette recette;

    public DetailsRecette(double quantite, Unite unite, Recette recette, Ingredient ingredient) {
        this.quantite = quantite;
        this.unite = unite;
        this.recette = recette;
        this.ingredient = ingredient;
    }

    @Override
    public String toString() {
        return "Ingredients{" +
                "id=" + id +
                ", quantite=" + quantite +
                ", recette=" + recette +
                ", ingredient=" + ingredient +
                '}';
    }
}
