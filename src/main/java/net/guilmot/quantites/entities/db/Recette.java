package net.guilmot.quantites.entities.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "recette")
public class Recette {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "nom", nullable = false, updatable = false)
    private String nom;
    @Column(name = "nb_adultes", nullable = false)
    private Integer nombreAdultes;
    @Column(name = "nb_enfants", nullable = false)
    private Integer nombreEnfants;
    @ManyToOne
    @JoinColumn(name = "id_plat")
    private Plat plat;
    @Column(name = "url")
    private String url;

    @OneToOne
    @JoinColumn(name = "id_scan")
    private Media scan;

    @OneToOne
    @JoinColumn(name = "id_photo")
    private Media photo;

    @Override
    public String toString() {
        return "Recette{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", nombreAdultes=" + nombreAdultes +
                ", nombreEnfants=" + nombreEnfants +
                ", plat=" + plat +
                ", url='" + url + '\'' +
                ", scan=" + scan +
                ", photo=" + photo +
                '}';
    }
}
