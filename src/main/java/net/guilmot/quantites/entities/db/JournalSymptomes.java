package net.guilmot.quantites.entities.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "journal_symptomes")
public class JournalSymptomes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "ts_crea", nullable = false)
    private String tsCrea;

    @Column(name = "force", nullable = false)
    private int force;

    @OneToOne
    @JoinColumn(name = "id_symptome")
    private Symptome symptome;

    @Override
    public String toString() {
        return "JournalSymptomes{" +
                "id=" + id +
                ", tsCrea='" + tsCrea + '\'' +
                ", force=" + force +
                ", symptome=" + symptome +
                '}';
    }
}
