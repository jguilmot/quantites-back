package net.guilmot.quantites.entities.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "unite")
public class Unite {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "nom", nullable = false, updatable = false)
    private String nom;
    @Column(name = "plural", nullable = false)
    private Boolean plural;
    @Column(name = "order", nullable = false)
    private Integer order;

    public Unite(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Unite{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", plural=" + plural +
                ", order=" + order +
                '}';
    }
}
