package net.guilmot.quantites.entities.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "journal_alimentaire")
public class JournalAlimentaire {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "ts_crea", nullable = false)
    private String tsCrea;

    @Column(name = "quantite", nullable = false)
    private double quantite;

    @OneToOne
    @JoinColumn(name = "id_unite")
    private Unite unite;

    @OneToOne
    @JoinColumn(name = "id_ingredient")
    private Ingredient ingredient;

    @Override
    public String toString() {
        return "JournalAlimentaire{" +
                "id=" + id +
                ", tsCrea='" + tsCrea + '\'' +
                ", quantite=" + quantite +
                ", unite=" + unite +
                ", ingredient=" + ingredient +
                '}';
    }
}
