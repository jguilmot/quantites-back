package net.guilmot.quantites.entities.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "invariable")
public class Invariable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "nom", nullable = false, unique = true)
    private String nom;
    @Column(name = "plural", nullable = false)
    private Boolean plural;

    public Invariable(String nom, Boolean plural) {
        this.nom = nom;
        this.plural=plural;
    }

    @Override
    public String toString() {
        return "Invariable{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", plural=" + plural +
                '}';
    }
}

