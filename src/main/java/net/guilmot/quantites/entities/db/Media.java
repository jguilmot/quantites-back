package net.guilmot.quantites.entities.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.util.Arrays;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "media")
public class Media {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Lob
    @Column(name = "fichier", nullable = false)
    @Type(type = "org.hibernate.type.BinaryType")
    private byte[] fichier;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "nom", nullable = false)
    private String nom;

    @Override
    public String toString() {
        return "Media{" +
                "id=" + id +
                ", fichier=" + Arrays.toString(fichier) +
                ", type='" + type + '\'' +
                ", nom='" + nom + '\'' +
                '}';
    }
}
