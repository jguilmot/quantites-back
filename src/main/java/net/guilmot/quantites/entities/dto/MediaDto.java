package net.guilmot.quantites.entities.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MediaDto {
    private Integer id;
    private byte[] fichier;
    private String type;
    private String nom;
}
