package net.guilmot.quantites.entities.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.guilmot.quantites.entities.db.Plat;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RecetteDto {
    private Integer id;
    private String nom;
    private Integer nombreAdultes;
    private Integer nombreEnfants;
    private Plat plat;
    private String url;
    private byte[] photo;
    private byte[] scan;
}
