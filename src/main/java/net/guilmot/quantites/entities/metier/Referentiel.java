package net.guilmot.quantites.entities.metier;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Referentiel {
    private ReferentielParametrage referentielParametrage;
    private ReferentielMetier referentielMetier;

}
