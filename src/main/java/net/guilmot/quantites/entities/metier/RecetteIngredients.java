package net.guilmot.quantites.entities.metier;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.guilmot.quantites.entities.db.Recette;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RecetteIngredients {
    private Recette recette;
    private List<DetailIngredient> detailIngredients;

    @Override
    public String toString() {
        return "RecetteIngredients{" +
                "recette=" + recette +
                ", detailIngredients=" + detailIngredients +
                '}';
    }
}
