package net.guilmot.quantites.entities.metier;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.guilmot.quantites.entities.db.Invariable;
import net.guilmot.quantites.entities.db.Unite;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ReferentielParametrage {
    private List<Invariable> invariables;
    private List<Unite> unites;
}
