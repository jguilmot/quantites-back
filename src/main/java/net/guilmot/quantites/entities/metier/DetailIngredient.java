package net.guilmot.quantites.entities.metier;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.guilmot.quantites.entities.db.Ingredient;
import net.guilmot.quantites.entities.db.Unite;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DetailIngredient {

    private double quantite;
    private Unite unite;
    private Ingredient ingredient;

    @Override
    public String toString() {
        return "DetailIngredient{" +
                "quantite=" + quantite +
                ", unite=" + unite +
                ", ingredient=" + ingredient +
                '}';
    }
}
