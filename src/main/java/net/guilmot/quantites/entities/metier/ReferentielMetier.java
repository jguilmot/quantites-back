package net.guilmot.quantites.entities.metier;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.guilmot.quantites.entities.db.Ingredient;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ReferentielMetier {
    private List<RecetteIngredients> recettesIngredients;
    private List<Ingredient> ingredients;

}
