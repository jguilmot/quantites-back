package net.guilmot.quantites.controller.metier;

import net.guilmot.quantites.entities.metier.RecetteIngredients;
import net.guilmot.quantites.services.db.DetailsRecetteService;
import net.guilmot.quantites.services.metier.ReferentielService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/recettes-ingredients")
public class RecettesIngredientsController {
    @Autowired
    private DetailsRecetteService detailsRecetteService;

    @Autowired
    private ReferentielService referentielService;

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<RecetteIngredients> recettesIngredientsAll() {
        return detailsRecetteService.findRecettesIngredientsAll();
    }

    @GetMapping("/recette/nom/{nom}")
    @ResponseStatus(HttpStatus.OK)
    public List<RecetteIngredients> recettesIngredientsByRecetteNom(@PathVariable final String nom) {
        return detailsRecetteService.findRecettesIngredientsByRecetteNom(nom);
    }

    @GetMapping("/recette/nom/contains/{nom}/ignore-case")
    @ResponseStatus(HttpStatus.OK)
    public List<RecetteIngredients> recettesIngredientsByRecetteNomContainingIgnoreCase(@PathVariable final String nom) {
        return detailsRecetteService.findRecettesIngredientsByRecetteNomContainingIgnoreCase(nom);
    }

    @GetMapping("/recette-or-ingredient/nom/contains/{chaine}/ignore-case")
    @ResponseStatus(HttpStatus.OK)
    public List<RecetteIngredients> recettesIngredientsByRecetteNomOrIngredientNomContainingIgnoreCase(@PathVariable final String chaine) {
        return detailsRecetteService.findRecettesIngredientsByRecetteNomOrIngredientNomContainingIgnoreCase(chaine);
    }





}
