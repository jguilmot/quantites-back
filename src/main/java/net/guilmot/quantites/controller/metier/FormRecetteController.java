package net.guilmot.quantites.controller.metier;

import lombok.extern.slf4j.Slf4j;
import net.guilmot.quantites.entities.metier.FormRecette;
import net.guilmot.quantites.services.metier.FormRecetteService;
import net.guilmot.quantites.services.metier.ReferentielMetierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/form-recette")
public class FormRecetteController {

    @Autowired
    private FormRecetteService formRecetteService;

    @Autowired
    private ReferentielMetierService referentielMetierService;

    @PostMapping("/create-or-update")
    @ResponseStatus(HttpStatus.CREATED)
    public FormRecette createOrUpdateRecette(@RequestBody FormRecette formRecette) {
        FormRecette formRecette1 = formRecetteService.createOrUpdate(formRecette);
        referentielMetierService.initReferentiel();
        return formRecette1;
    }

    @DeleteMapping("/delete/id/{idRecette}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteRecetteById(@PathVariable final Integer idRecette) {
        formRecetteService.delete(idRecette);
        referentielMetierService.initReferentiel();
    }
}
