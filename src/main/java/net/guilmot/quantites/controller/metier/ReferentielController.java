package net.guilmot.quantites.controller.metier;

import lombok.extern.slf4j.Slf4j;
import net.guilmot.quantites.entities.metier.Referentiel;
import net.guilmot.quantites.entities.metier.ReferentielMetier;
import net.guilmot.quantites.entities.metier.ReferentielParametrage;
import net.guilmot.quantites.services.metier.ReferentielService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/referentiel")
public class ReferentielController {

    @Autowired
    private ReferentielService referentielService;

    @GetMapping(value = "/all/{forceReloadParametrage}/{forceReloadMetier}")
    @ResponseStatus(HttpStatus.OK)
    public Referentiel referentielAll(@PathVariable boolean forceReloadParametrage, @PathVariable boolean forceReloadMetier) {
        return referentielService.getReferentielAll(forceReloadParametrage, forceReloadMetier);
    }

    @GetMapping(value = "/parametrage/all/{forceReload}")
    @ResponseStatus(HttpStatus.OK)
    public ReferentielParametrage referentielParametrageAll(@PathVariable boolean forceReload) {
        return referentielService.getReferentielParametrage(forceReload);
    }

    @GetMapping(value = "/metier/all/{forceReload}")
    @ResponseStatus(HttpStatus.OK)
    public ReferentielMetier referentielMetierAll(@PathVariable boolean forceReload) {
        return referentielService.getReferentielMetier(forceReload);
    }


}
