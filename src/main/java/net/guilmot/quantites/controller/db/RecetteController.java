package net.guilmot.quantites.controller.db;

import net.guilmot.quantites.entities.db.Plat;
import net.guilmot.quantites.entities.db.Recette;
import net.guilmot.quantites.entities.dto.PlatDto;
import net.guilmot.quantites.services.db.DetailsRecetteService;
import net.guilmot.quantites.services.db.RecetteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/recettes")
public class RecetteController {

    @Autowired
    private RecetteService recetteService;

    @Autowired
    private DetailsRecetteService detailsRecetteService;

    @GetMapping("/id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Recette recetteById(@PathVariable final Integer id) {
        return recetteService.findById(id);
    }

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<Recette> recettesAll() {
        return recetteService.findAll();
    }

    @GetMapping("/nom/{nom}")
    @ResponseStatus(HttpStatus.OK)
    public List<Recette> recettesByNom(@PathVariable final String nom) {
        return recetteService.findByNom(nom);
    }

    @GetMapping("/plat/nom/{nomPlat}")
    @ResponseStatus(HttpStatus.OK)
    public List<Recette> recettesByPlatNom(@PathVariable final String nomPlat) {
        return recetteService.findByPlatNom(nomPlat);
    }

    @GetMapping("/plat/id/{idPlat}")
    @ResponseStatus(HttpStatus.OK)
    public List<Recette> recettesByPlatId(@PathVariable final Integer idPlat) {
        return recetteService.findByPlatId(idPlat);
    }

    @PostMapping("/plat/object")
    @ResponseStatus(HttpStatus.OK)
    public List<Recette> recettesByPlat(@RequestBody final PlatDto platDto) {

        Plat plat = new Plat();
        plat.setId(platDto.getId());
        plat.setNom(platDto.getNom());

        return recetteService.findByPlat(plat);
    }

    @GetMapping("/ingredient/id/{idIngredient}")
    @ResponseStatus(HttpStatus.OK)
    public List<Recette> recettesByIngredientId(@PathVariable final Integer idIngredient) {
        return detailsRecetteService.findRecettesByIngredientId(idIngredient);
    }

    @GetMapping("/ingredient/nom/{nomIngredient}")
    @ResponseStatus(HttpStatus.OK)
    public List<Recette> recettesByIngredientNom(@PathVariable final String nomIngredient) {
        return detailsRecetteService.findRecettesByIngredientNom(nomIngredient);
    }

    @PostMapping("/ingredients/nom/")
    @ResponseStatus(HttpStatus.OK)
    public List<Recette> recettesByIngredientsNom(@RequestBody final List<String> nomIngredients) {
        return detailsRecetteService.findRecettesByIngredientsNom(nomIngredients);
    }
}

