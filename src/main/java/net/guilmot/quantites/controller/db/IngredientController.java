package net.guilmot.quantites.controller.db;

import net.guilmot.quantites.entities.db.Ingredient;
import net.guilmot.quantites.services.db.DetailsRecetteService;
import net.guilmot.quantites.services.db.IngredientService;
import net.guilmot.quantites.services.metier.ReferentielMetierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/ingredients")
public class IngredientController {
    @Autowired
    private IngredientService ingredientService;

    @Autowired
    private DetailsRecetteService detailsRecetteService;

    @Autowired
    private ReferentielMetierService referentielMetierService;

    @GetMapping("/id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Ingredient ingredientById(@PathVariable final Integer id) {
        return ingredientService.findById(id);
    }

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<Ingredient> ingredientsAll() {
        return ingredientService.findAll();
    }

    @GetMapping("/nom/{nom}")
    @ResponseStatus(HttpStatus.OK)
    public List<Ingredient> ingredientsByNom(@PathVariable final String nom) {
        return ingredientService.findByNom(nom);
    }

    @PostMapping("/create-or-update")
    @ResponseStatus(HttpStatus.CREATED)
    public List<Ingredient> createOrUpdateIngredients(@RequestBody List<Ingredient> ingredients) {
        List<Ingredient> ingredientsUpdated = ingredientService.createOrUpdate(ingredients);
        referentielMetierService.initReferentiel();
        return ingredientsUpdated;
    }

    @PostMapping("/update")
    @ResponseStatus(HttpStatus.CREATED)
    public List<Ingredient> updateIngredients(@RequestBody List<Ingredient> ingredients) {

        List<Ingredient> ingredientsUpdated = ingredientService.update(ingredients);
        referentielMetierService.initReferentiel();
        return ingredientsUpdated;
    }

    @GetMapping("/recette/id/{idRecette}")
    @ResponseStatus(HttpStatus.OK)
    public List<Ingredient> ingredientsByRecetteId(@PathVariable final Integer idRecette) {
        return detailsRecetteService.findIngredientsByRecetteId(idRecette);
    }

    @GetMapping("/recette/nom/{nomRecette}")
    @ResponseStatus(HttpStatus.OK)
    public List<Ingredient> ingredientsByRecetteNom(@PathVariable final String nomRecette) {
        return detailsRecetteService.findIngredientsByRecetteNom(nomRecette);
    }

}
