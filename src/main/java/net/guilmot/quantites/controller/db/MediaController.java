package net.guilmot.quantites.controller.db;

import lombok.extern.slf4j.Slf4j;
import net.guilmot.quantites.entities.db.Media;
import net.guilmot.quantites.entities.dto.MediaDto;
import net.guilmot.quantites.services.db.MediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/medias")
public class MediaController {

    @Autowired
    private MediaService mediaService;

    @GetMapping("/id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Media mediaById(@PathVariable final Integer id) {
        return mediaService.findById(id);
    }

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<Media> mediasAll() {
        return mediaService.findAll();
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public Media create(@RequestBody final MediaDto mediaDto) {

        Media media = new Media();

        media.setId(mediaDto.getId());
        media.setFichier(mediaDto.getFichier());
        media.setType(mediaDto.getType());
        media.setNom(mediaDto.getNom());

        return mediaService.create(media);
    }

    @PostMapping("/upload")
    @ResponseStatus(HttpStatus.CREATED)
    public Media create(@RequestParam("file") MultipartFile file, @RequestParam("type") String type) throws IOException {

        Media media = new Media();

        media.setFichier(file.getBytes());
        media.setNom(file.getOriginalFilename());
        media.setType(type);

        return mediaService.create(media);
    }
}
