package net.guilmot.quantites.controller.db;

import net.guilmot.quantites.entities.db.Plat;
import net.guilmot.quantites.services.db.PlatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/plats")
public class PlatController {

    @Autowired
    private PlatService platService;

    @GetMapping("/id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Plat platById(@PathVariable final Integer id) {
        return platService.findById(id);
    }

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<Plat> platsAll() {
        return platService.findAll();
    }

    @GetMapping("/nom/{nom}")
    @ResponseStatus(HttpStatus.OK)
    public List<Plat> platsByNom(@PathVariable final String nom) {
        return platService.findByNom(nom);
    }
}
