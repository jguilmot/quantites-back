package net.guilmot.quantites.controller.db;

import net.guilmot.quantites.entities.db.DetailsRecette;
import net.guilmot.quantites.entities.db.Ingredient;
import net.guilmot.quantites.entities.db.Recette;
import net.guilmot.quantites.entities.dto.IngredientDto;
import net.guilmot.quantites.entities.dto.RecetteDto;
import net.guilmot.quantites.services.db.DetailsRecetteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/details-recette")
public class DetailsRecetteController {

    @Autowired
    private DetailsRecetteService detailsRecetteService;

    @GetMapping("/id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public DetailsRecette detailsRecetteById(@PathVariable final Integer id) {
        return detailsRecetteService.findById(id);
    }

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<DetailsRecette> detailsRecetteAll() {
        return detailsRecetteService.findAll();
    }

    @GetMapping("/recette/nom/{nom}")
    @ResponseStatus(HttpStatus.OK)
    public List<DetailsRecette> detailsRecetteByRecetteNom(@PathVariable final String nom) {
        return detailsRecetteService.findByRecetteNom(nom);
    }

    @GetMapping("/recette/nom/contains/{nom}/ignore-case")
    @ResponseStatus(HttpStatus.OK)
    public List<DetailsRecette> detailsRecetteByRecetteNomContainingIgnoreCase(@PathVariable final String nom) {
        return detailsRecetteService.findByRecetteNomContainingIgnoreCase(nom);
    }

    @GetMapping("/recette/id/{idRecette}")
    @ResponseStatus(HttpStatus.OK)
    public List<DetailsRecette> detailsRecetteByRecetteId(@PathVariable final Integer idRecette) {
        return detailsRecetteService.findByRecetteId(idRecette);
    }

    @PostMapping("/recette/object")
    @ResponseStatus(HttpStatus.OK)
    public List<DetailsRecette> detailsRecetteByRecette(@RequestBody final RecetteDto recetteDto) {

        Recette recette = new Recette();
        recette.setId(recetteDto.getId());
        recette.setNom(recetteDto.getNom());
        recette.setNombreAdultes(recetteDto.getNombreAdultes());
        recette.setNombreEnfants(recetteDto.getNombreEnfants());
        recette.setUrl(recetteDto.getUrl());
        recette.setPlat(recetteDto.getPlat());

        return detailsRecetteService.findByRecette(recette);
    }

    @GetMapping("/ingredient/nom/{nom}")
    @ResponseStatus(HttpStatus.OK)
    public List<DetailsRecette> detailsRecetteByIngredientNom(@PathVariable final String nom) {
        return detailsRecetteService.findByIngredientNom(nom);
    }

    @GetMapping("/ingredient/id/{idIngredient}")
    @ResponseStatus(HttpStatus.OK)
    public List<DetailsRecette> detailsRecetteByIngredientId(@PathVariable final Integer idIngredient) {

        return detailsRecetteService.findByIngredientId(idIngredient);
    }

    @PostMapping("/ingredient/object")
    @ResponseStatus(HttpStatus.OK)
    public List<DetailsRecette> detailsRecetteByIngredient(@RequestBody final IngredientDto ingredientDto) {

        Ingredient ingredient = new Ingredient();
        ingredient.setId(ingredientDto.getId());
        ingredient.setNom(ingredientDto.getNom());

        return detailsRecetteService.findByIngredient(ingredient);
    }

}
