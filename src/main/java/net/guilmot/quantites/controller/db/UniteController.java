package net.guilmot.quantites.controller.db;

import net.guilmot.quantites.entities.db.Unite;
import net.guilmot.quantites.services.db.UniteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/unites")
public class UniteController {
    @Autowired
    private UniteService uniteService;

    @GetMapping("/id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Unite uniteById(@PathVariable final Integer id) {
        return uniteService.findById(id);
    }

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<Unite> unitesAll() {
        return uniteService.findAll();
    }

    @GetMapping("/nom/{nom}")
    @ResponseStatus(HttpStatus.OK)
    public List<Unite> unitesByNom(@PathVariable final String nom) {
        return uniteService.findByNom(nom);
    }


}
