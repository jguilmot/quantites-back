package net.guilmot.quantites.controller.db;

import lombok.extern.slf4j.Slf4j;
import net.guilmot.quantites.entities.db.Invariable;
import net.guilmot.quantites.services.db.InvariableService;
import net.guilmot.quantites.services.metier.ReferentielParametrageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/invariables")
@Slf4j
public class InvariableController {
    @Autowired
    private InvariableService invariableService;

    @Autowired
    private ReferentielParametrageService referentielParametrageService;

    @GetMapping("/id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Invariable invariableById(@PathVariable final Integer id) {
        return invariableService.findById(id);
    }

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<Invariable> invariablesAll() {
        return invariableService.findAll();
    }

    @GetMapping("/nom/{nom}")
    @ResponseStatus(HttpStatus.OK)
    public List<Invariable> invariablesByNom(@PathVariable final String nom) {
        return invariableService.findByNom(nom);
    }

    @PostMapping("/create-or-update")
    @ResponseStatus(HttpStatus.CREATED)
    public List<Invariable> createOrUpdateInvariables(@RequestBody List<Invariable> invariables) {
        List<Invariable> invariablesUpdated = invariableService.createOrUpdate(invariables);
        referentielParametrageService.initReferentiel();
        return invariablesUpdated;
    }


}
