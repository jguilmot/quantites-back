package net.guilmot.quantites.services.db;

import net.guilmot.quantites.entities.db.Invariable;

import java.util.List;

public interface InvariableService {
    Invariable create(Invariable i);

    Invariable findById(int id);

    List<Invariable> findByNom(String nom);

    List<Invariable> findAll();

    void deleteAll();

    void delete(Invariable invariable);

    List<Invariable> createOrUpdate(List<Invariable> invariables);
}
