package net.guilmot.quantites.services.db;

import net.guilmot.quantites.entities.db.DetailsRecette;
import net.guilmot.quantites.entities.db.Ingredient;
import net.guilmot.quantites.entities.db.Recette;
import net.guilmot.quantites.entities.metier.RecetteIngredients;

import java.util.List;

public interface DetailsRecetteService {

    void delete (DetailsRecette dr);

    DetailsRecette create(DetailsRecette dr);

    DetailsRecette findById(int id);

    List<DetailsRecette> findByRecette(Recette r);

    List<DetailsRecette> findByRecetteNom(String nom);

    List<DetailsRecette> findByRecetteNomContainingIgnoreCase(String nom);
    List<DetailsRecette> findByIngredientNomContainingIgnoreCase(String nom);

    List<DetailsRecette> findByRecetteId(int id);

    List<DetailsRecette> findByIngredient(Ingredient i);

    List<DetailsRecette> findByIngredientNom(String nom);

    List<DetailsRecette> findByIngredientId(int id);

    List<DetailsRecette> findAll();

    List<Ingredient> findIngredientsByRecetteNom(String nom);

    List<Ingredient> findIngredientsByRecetteId(int id);

    List<Recette> findRecettesByIngredientId(int id);

    List<Recette> findRecettesByIngredientsNom(List<String> noms);

    List<RecetteIngredients> findRecettesIngredientsAll();

    List<RecetteIngredients> findRecettesIngredientsByRecetteNom(String nom);

    List<RecetteIngredients> findRecettesIngredientsByRecetteNomContainingIgnoreCase(String nom);

    List<RecetteIngredients> findRecettesIngredientsByRecetteNomOrIngredientNomContainingIgnoreCase(String chaine);

    List<Recette> findRecettesByIngredientNom(String nom);

}
