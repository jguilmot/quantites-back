package net.guilmot.quantites.services.db.impl;

import net.guilmot.quantites.dao.IngredientDao;
import net.guilmot.quantites.entities.db.Ingredient;
import net.guilmot.quantites.entities.db.Invariable;
import net.guilmot.quantites.services.db.IngredientService;
import net.guilmot.quantites.services.metier.ReferentielParametrageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service("ingredientService")
public class IngredientServiceImpl implements IngredientService {

    private static final int NB_MAX_SPLIT = 2;

    @Autowired
    private IngredientDao ingredientDao;

    @Override
    public Ingredient create(Ingredient i) {
        return ingredientDao.save(i);
    }

    @Override
    public Ingredient findById(int id) {
        return ingredientDao.findById(id).orElse(null);
    }

    @Override
    public List<Ingredient> findByNom(String nom) {
        return ingredientDao.findByNom(nom);
    }

    @Override
    public List<Ingredient> findByNomContainingIgnoreCase(String nom) {
        return ingredientDao.findByNomContainingIgnoreCase(nom);
    }

    @Override
    public List<Ingredient> findAll() {
        return ingredientDao.findAll(Sort.by(Sort.Direction.ASC, "nom"));
    }

    @Override
    public void deleteAll() {
        ingredientDao.deleteAll();
    }



    @Override
    public List<Ingredient> createOrUpdate(List<Ingredient> i) {
        i.forEach(this::create);
        return i;
    }

    @Override
    public List<Ingredient> update(List<Ingredient> i) {
        i.forEach(ingredient -> {
            ingredient.setNom(formatIngredient(ingredient.getNom()));
            List<Ingredient> ingredientToUpdate = findByNom(ingredient.getNom());
            if (!ingredientToUpdate.isEmpty()) {
                ingredientToUpdate.get(0).setNom(ingredient.getNom());
                create(ingredientToUpdate.get(0));
            } else {
                create(ingredient);
            }
        });
        return i;
    }

    @Override
    public String formatIngredient(String nomIngredient) {


        List<String> invariablesPlural = ReferentielParametrageService.getREFERENTIEL_PARAMETRAGE().getInvariables().stream().filter(Invariable::getPlural).map(Invariable::getNom).toList();

        // Retrait des espaces avant et après
        nomIngredient = nomIngredient.trim();

        // Mise en minuscule
        nomIngredient = nomIngredient.toLowerCase(Locale.FRENCH);

        // Capitalisation du premier caractère
        nomIngredient = nomIngredient.substring(0, 1).toUpperCase(Locale.FRENCH) + nomIngredient.substring(1);

        String[] wordsIngredient = nomIngredient.trim().split(" ", NB_MAX_SPLIT);
        String firstWordIngredient = wordsIngredient[0];

        // Retrait des 's' ou 'x' sauf pour les exceptions 'invariable Plural'
        if (!invariablesPlural.contains(firstWordIngredient) && (
                "s".equals(firstWordIngredient.substring(firstWordIngredient.length() - 1)) ||
                        "x".equals(firstWordIngredient.substring(firstWordIngredient.length() - 1)))
        ) {
            nomIngredient = firstWordIngredient.substring(0, firstWordIngredient.length() - 1) + nomIngredient.substring(firstWordIngredient.length());
            if ("s".equals(nomIngredient.substring(nomIngredient.length() - 1)) ||
                    "x".equals(nomIngredient.substring(nomIngredient.length() - 1))) {
                nomIngredient = nomIngredient.substring(0, nomIngredient.length() - 1);
            }
        }

        // Ajout du 's' si non saisi pour les invariables Plural
        if (invariablesPlural.contains(nomIngredient + 's')) {
            nomIngredient = nomIngredient + 's';
        }

        return nomIngredient;


    }

}
