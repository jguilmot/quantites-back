package net.guilmot.quantites.services.db.impl;

import net.guilmot.quantites.dao.DetailsRecetteDao;
import net.guilmot.quantites.entities.db.DetailsRecette;
import net.guilmot.quantites.entities.db.Ingredient;
import net.guilmot.quantites.entities.db.Invariable;
import net.guilmot.quantites.entities.db.Recette;
import net.guilmot.quantites.entities.db.Unite;
import net.guilmot.quantites.entities.metier.DetailIngredient;
import net.guilmot.quantites.entities.metier.RecetteIngredients;
import net.guilmot.quantites.services.db.DetailsRecetteService;
import net.guilmot.quantites.services.db.IngredientService;
import net.guilmot.quantites.services.db.RecetteService;
import net.guilmot.quantites.services.metier.ReferentielParametrageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service("DetailsRecetteService")
public class DetailsRecetteServiceImpl implements DetailsRecetteService {

    private static final double TX_MINIMUM_SELECTION_RECETTE = 0.6;
    private static final int NB_MAX_SPLIT = 2;

    @Autowired
    private DetailsRecetteDao detailsRecetteDao;

    @Autowired
    private RecetteService recetteService;

    @Autowired
    private IngredientService ingredientService;

    @Override
    public void delete(DetailsRecette dr) {
        detailsRecetteDao.delete(dr);
    }

    @Override
    public DetailsRecette create(DetailsRecette dr) {
        return detailsRecetteDao.save(dr);
    }

    @Override
    public DetailsRecette findById(int id) {
        return detailsRecetteDao.findById(id).orElse(null);
    }

    @Override
    public List<DetailsRecette> findByRecette(Recette r) {
        return detailsRecetteDao.findByRecette(r);
    }

    @Override
    public List<DetailsRecette> findByRecetteNom(String s) {

        List<DetailsRecette> detailsRecetteByRecetteNom = new ArrayList<>();
        List<DetailsRecette> detailsRecetteByRecetteId;

        List<Recette> recettes = recetteService.findByNom(s);
        for (Recette recette : recettes
        ) {
            detailsRecetteByRecetteId = detailsRecetteDao.findByRecetteId(recette.getId());
            detailsRecetteByRecetteNom.addAll(detailsRecetteByRecetteId);
        }
        return detailsRecetteByRecetteNom;
    }

    @Override
    public List<DetailsRecette> findByRecetteNomContainingIgnoreCase(String s) {

        List<DetailsRecette> detailsRecetteByRecetteNom = new ArrayList<>();
        List<DetailsRecette> detailsRecetteByRecetteId;

        List<Recette> recettes = recetteService.findByNomContainingIgnoreCase(s);
        for (Recette recette : recettes
        ) {
            detailsRecetteByRecetteId = detailsRecetteDao.findByRecetteId(recette.getId());
            detailsRecetteByRecetteNom.addAll(detailsRecetteByRecetteId);
        }
        return detailsRecetteByRecetteNom;
    }

    @Override
    public List<DetailsRecette> findByRecetteId(int idRecette) {
        return detailsRecetteDao.findByRecetteId(idRecette);
    }

    @Override
    public List<DetailsRecette> findByIngredient(Ingredient i) {
        return detailsRecetteDao.findByIngredient(i);
    }

    @Override
    public List<DetailsRecette> findByIngredientNom(String s) {

        List<DetailsRecette> detailsRecetteByIngredientNom = new ArrayList<>();
        List<DetailsRecette> detailsRecetteByIngredientId;

        List<Ingredient> ingredients = ingredientService.findByNom(s);
        for (Ingredient ingredient : ingredients
        ) {
            detailsRecetteByIngredientId = detailsRecetteDao.findByIngredientId(ingredient.getId());
            detailsRecetteByIngredientNom.addAll(detailsRecetteByIngredientId);
        }
        return detailsRecetteByIngredientNom;
    }

    @Override
    public List<DetailsRecette> findByIngredientNomContainingIgnoreCase(String s) {

        List<DetailsRecette> detailsRecetteByIngredientNom = new ArrayList<>();
        List<DetailsRecette> detailsRecetteByIngredientId;

        List<Ingredient> ingredients = ingredientService.findByNomContainingIgnoreCase(s);
        for (Ingredient ingredient : ingredients
        ) {
            detailsRecetteByIngredientId = detailsRecetteDao.findByIngredientId(ingredient.getId());
            detailsRecetteByIngredientNom.addAll(detailsRecetteByIngredientId);
        }
        return detailsRecetteByIngredientNom;
    }

    @Override
    public List<DetailsRecette> findByIngredientId(int idIngredient) {
        return detailsRecetteDao.findByIngredientId(idIngredient);
    }

    @Override
    public List<DetailsRecette> findAll() {
        return detailsRecetteDao.findAll();
    }

    private static List<RecetteIngredients> getRecetteIngredients(List<DetailsRecette> detailsRecettes) {

        List<RecetteIngredients> recettesIngredients = new ArrayList<>();
        RecetteIngredients recetteIngredients = new RecetteIngredients();

        List<DetailIngredient> detailsIngredient = new ArrayList<>();
        int idRecette = 0;

        for (DetailsRecette detailsRecette : detailsRecettes) {

            if (idRecette != detailsRecette.getRecette().getId()) {
                if (idRecette != 0) {
                    detailsIngredient = detailsIngredient.stream().sorted(Comparator.comparing(di -> di.getIngredient().getNom())).collect(Collectors.toList());
                    recetteIngredients.setDetailIngredients(detailsIngredient);
                    recettesIngredients.add(new RecetteIngredients(recetteIngredients.getRecette(), new ArrayList<>(recetteIngredients.getDetailIngredients())));
                    detailsIngredient.clear();
                }
                recetteIngredients.setRecette(detailsRecette.getRecette());
                idRecette = detailsRecette.getRecette().getId();
            }

            detailsIngredient.add(new DetailIngredient(detailsRecette.getQuantite(), new Unite(detailsRecette.getUnite().getId(), detailsRecette.getUnite().getNom(), detailsRecette.getUnite().getPlural(), detailsRecette.getUnite().getOrder()), new Ingredient(detailsRecette.getIngredient().getId(), detailsRecette.getIngredient().getNom())));


        }

        detailsIngredient = detailsIngredient.stream().sorted(Comparator.comparing(di -> di.getIngredient().getNom())).collect(Collectors.toList());
        recetteIngredients.setDetailIngredients(detailsIngredient);
        recettesIngredients.add(new RecetteIngredients(recetteIngredients.getRecette(), new ArrayList<>(recetteIngredients.getDetailIngredients())));

        return recettesIngredients;
    }

    @Override
    public List<Ingredient> findIngredientsByRecetteNom(String nomRecette) {

        List<DetailsRecette> detailsRecetteByRecetteId;
        List<Ingredient> ingredientsByRecetteNom = new ArrayList<>();

        List<Recette> recettes = recetteService.findByNom(nomRecette);

        for (Recette recette : recettes) {
            detailsRecetteByRecetteId = findByRecetteId(recette.getId());
            for (DetailsRecette detailsRecette : detailsRecetteByRecetteId) {
                if (!ingredientsByRecetteNom.contains(detailsRecette.getIngredient())) {
                    ingredientsByRecetteNom.add(detailsRecette.getIngredient());
                }
            }
        }

        return ingredientsByRecetteNom;
    }

    @Override
    public List<Ingredient> findIngredientsByRecetteId(int idRecette) {

        List<DetailsRecette> detailsRecetteByRecetteId;
        List<Ingredient> ingredientsByRecetteId = new ArrayList<>();

        detailsRecetteByRecetteId = findByRecetteId(idRecette);
        for (DetailsRecette detailsRecette : detailsRecetteByRecetteId) {
            if (!ingredientsByRecetteId.contains(detailsRecette.getIngredient())) {
                ingredientsByRecetteId.add(detailsRecette.getIngredient());
            }
        }

        return ingredientsByRecetteId;

    }

    @Override
    public List<Recette> findRecettesByIngredientId(int idIngredient) {

        List<DetailsRecette> detailsRecetteByIngredientId;
        List<Recette> recettesByIngredientId = new ArrayList<>();

        detailsRecetteByIngredientId = findByIngredientId(idIngredient);
        for (DetailsRecette detailsRecette : detailsRecetteByIngredientId) {
            if (!recettesByIngredientId.contains(detailsRecette.getRecette())) {
                recettesByIngredientId.add(detailsRecette.getRecette());
            }
        }

        return recettesByIngredientId;

    }

    @Override
    public List<Recette> findRecettesByIngredientsNom(List<String> nomIngredients) {

        List<Ingredient> ingredients = new ArrayList<>();
        List<Ingredient> ingredientsByNom;
        List<Ingredient> ingredientsByRecetteId;
        List<Recette> recettesByIngredientsNom = new ArrayList<>();
        List<Recette> recettes;
        int ingredientsFound;
        double ingredientsMinimum;

        // Construction de la liste des ingredients passés en paramètre
        // (à partir de la liste des String nomIngredients)
        for (String nomIngredient : nomIngredients) {
            ingredientsByNom = ingredientService.findByNom(nomIngredient);
            ingredients.addAll(ingredientsByNom);
        }

        // On calcule le nombre d'ingrédients minimum pour retenir une recette
        ingredientsMinimum = ingredients.size() * TX_MINIMUM_SELECTION_RECETTE;

        // On récupère toutes les recettes et les ingrédients associés
        recettes = recetteService.findAll();
        for (Recette recette : recettes) {
            ingredientsByRecetteId = findIngredientsByRecetteId(recette.getId());
            ingredientsFound = 0;
            // On compare la liste passée en paramètre et celle de toutes les recettes / ingredients de la base
            for (Ingredient ingredient : ingredients) {
                if (ingredientsByRecetteId.contains(ingredient)) {
                    ingredientsFound++;
                }
            }
            // Si une recette contient au moins un ingrédient de la liste passée en paramètre
            // on la garde
            if (ingredientsFound >= ingredientsMinimum) {
                recettesByIngredientsNom.add(recette);
            }
        }

        return recettesByIngredientsNom;

    }

    @Override
    public List<RecetteIngredients> findRecettesIngredientsAll() {

        return formatAnswer(getRecetteIngredients(findAll().stream().distinct().sorted(Comparator.comparing(dr -> dr.getRecette().getNom())).toList()));

    }

    @Override
    public List<RecetteIngredients> findRecettesIngredientsByRecetteNom(String nom) {

        return formatAnswer(getRecetteIngredients(findByRecetteNom(nom.trim())));

    }

    @Override
    public List<RecetteIngredients> findRecettesIngredientsByRecetteNomContainingIgnoreCase(String nom) {

        return formatAnswer(getRecetteIngredients(findByRecetteNomContainingIgnoreCase(nom.trim())));

    }

    @Override
    public List<RecetteIngredients> findRecettesIngredientsByRecetteNomOrIngredientNomContainingIgnoreCase(String chaine) {

        List<DetailsRecette> detailsRecettesResult = new ArrayList<>();
        List<String> nomsIngredientSaisis = Arrays.asList(chaine.toLowerCase(Locale.ROOT).trim().split("\\s*,\\s*"));
        if (nomsIngredientSaisis.size() == 1) {
            detailsRecettesResult.addAll(findByRecetteNomContainingIgnoreCase(nomsIngredientSaisis.get(0).trim()));
            List<DetailsRecette> detailsRecettesByIngredientNom = findByIngredientNomContainingIgnoreCase(nomsIngredientSaisis.get(0).trim());
            for (DetailsRecette detailRecettesByIngredientNom : detailsRecettesByIngredientNom) {
                detailsRecettesResult.addAll(findByRecetteNom(detailRecettesByIngredientNom.getRecette().getNom()));
            }
        } else {
            for (Recette recette : recetteService.findAll()) {
                List<DetailsRecette> detailsRecettesByRecetteNom = findByRecetteNomContainingIgnoreCase((recette.getNom()));
                List<String> nomIngredients = detailsRecettesByRecetteNom.stream().map(dr -> dr.getIngredient().getNom().toLowerCase(Locale.ROOT)).toList();
                if (new HashSet<>(nomIngredients).containsAll(nomsIngredientSaisis)) {
                    detailsRecettesResult.addAll(detailsRecettesByRecetteNom);
                }
            }
        }
        return formatAnswer(getRecetteIngredients(detailsRecettesResult.stream().distinct().sorted(Comparator.comparing(dr -> dr.getRecette().getNom())).toList()));

    }

    private List<RecetteIngredients> formatAnswer(List<RecetteIngredients> recetteIngredients) {
        List<String> invariablesNonPlural = ReferentielParametrageService.getREFERENTIEL_PARAMETRAGE().getInvariables().stream().filter(i -> !i.getPlural()).map(
                Invariable::getNom).toList();
        for (RecetteIngredients recetteIngredient : recetteIngredients) {
            for (DetailIngredient detailIngredient : recetteIngredient.getDetailIngredients()) {
                List<String> unitesPlural = ReferentielParametrageService.getREFERENTIEL_PARAMETRAGE().getUnites().stream().filter(Unite::getPlural).map(Unite::getNom).toList();
                if (detailIngredient.getQuantite() > 1) {

                    if (unitesPlural.contains(detailIngredient.getUnite().getNom())) {
                        detailIngredient.getUnite().setNom(detailIngredient.getUnite().getNom() + 's');
                    }

                    String word = detailIngredient.getIngredient().getNom().trim().split(" ", NB_MAX_SPLIT)[0];
                    if (!invariablesNonPlural.contains(word) &&
                            "pièces".equals(detailIngredient.getUnite().getNom()) &&
                            !"s".equals(word.substring(word.length() - 1)) &&
                            !"x".equals(word.substring(word.length() - 1))) {
                        detailIngredient.getIngredient().setNom((detailIngredient.getIngredient().getNom().substring(0, word.length()) + "s" + detailIngredient.getIngredient().getNom().substring(word.length())).trim());
                    }

                }
            }
        }

        return recetteIngredients;
    }


    @Override
    public List<Recette> findRecettesByIngredientNom(String nomIngredient) {

        List<DetailsRecette> detailsRecetteByIngredientId;
        List<Recette> recettesByIngredientNom = new ArrayList<>();

        List<Ingredient> ingredients = ingredientService.findByNom(nomIngredient);

        for (Ingredient ingredient : ingredients) {
            detailsRecetteByIngredientId = findByIngredientId(ingredient.getId());
            for (DetailsRecette detailsRecette : detailsRecetteByIngredientId) {
                if (!recettesByIngredientNom.contains(detailsRecette.getRecette())) {
                    recettesByIngredientNom.add(detailsRecette.getRecette());
                }
            }
        }

        return recettesByIngredientNom;
    }


}
