package net.guilmot.quantites.services.db;

import net.guilmot.quantites.entities.db.Plat;
import net.guilmot.quantites.entities.db.Recette;

import java.util.List;


public interface RecetteService {

    void delete (Integer idRecette);

    Recette create(Recette r);

    Recette findById(int id);

    List<Recette> findByNom(String nom);

    List<Recette> findByNomContainingIgnoreCase(String nom);

    List<Recette> findAll();

    List<Recette> findByPlat(Plat plat);

    List<Recette> findByPlatNom(String nom);

    List<Recette> findByPlatId(int id);


}

