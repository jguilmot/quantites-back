package net.guilmot.quantites.services.db.impl;

import lombok.extern.slf4j.Slf4j;
import net.guilmot.quantites.dao.MediaDao;
import net.guilmot.quantites.entities.db.Media;
import net.guilmot.quantites.services.db.MediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service("mediaService")
public class MediaServiceImpl implements MediaService {

    @Autowired
    private MediaDao mediaDao;

    @Override
    @CacheEvict(value = "medias", allEntries = true)
    public Media create(Media m) {
        return mediaDao.save(m);
    }

    @Override
    @Cacheable("media")
    public Media findById(int id) {
        return mediaDao.findById(id).orElse(null);
    }

    @Override
    @Cacheable("medias")
    public List<Media> findAll() {
        return mediaDao.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

}
