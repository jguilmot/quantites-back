package net.guilmot.quantites.services.db.impl;

import net.guilmot.quantites.dao.PlatDao;
import net.guilmot.quantites.entities.db.Plat;
import net.guilmot.quantites.services.db.PlatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("platService")
public class PlatServiceImpl implements PlatService {
    @Autowired
    private PlatDao platDao;

    @Override
    public Plat create(Plat p) {
        return platDao.save(p);
    }

    @Override
    public Plat findById(int id) {
        return platDao.findById(id).orElse(null);
    }

    @Override
    public List<Plat> findByNom(String nom) {
        return platDao.findByNom(nom);
    }

    @Override
    public List<Plat> findAll() {
        return platDao.findAll();
    }
}
