package net.guilmot.quantites.services.db;

import net.guilmot.quantites.entities.db.Ingredient;

import java.util.List;

public interface IngredientService {
    Ingredient create(Ingredient i);


    Ingredient findById(int id);

    List<Ingredient> findByNom(String nom);

    List<Ingredient> findByNomContainingIgnoreCase(String nom);

    List<Ingredient> findAll();

    void deleteAll();

    List<Ingredient> createOrUpdate(List<Ingredient> i);

    List<Ingredient> update(List<Ingredient> ingredients);

    String formatIngredient(String nomIngredient);


}
