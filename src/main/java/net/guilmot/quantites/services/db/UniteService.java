package net.guilmot.quantites.services.db;

import net.guilmot.quantites.entities.db.Unite;

import java.util.List;

public interface UniteService {
    Unite create(Unite u);

    Unite findById(int id);

    List<Unite> findByNom(String nom);

    List<Unite> findAll();
}
