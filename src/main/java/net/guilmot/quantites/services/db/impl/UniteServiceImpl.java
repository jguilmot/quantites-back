package net.guilmot.quantites.services.db.impl;

import net.guilmot.quantites.dao.UniteDao;
import net.guilmot.quantites.entities.db.Unite;
import net.guilmot.quantites.services.db.UniteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("uniteService")
public class UniteServiceImpl implements UniteService {

    @Autowired
    private UniteDao uniteDao;

    @Override
    @CacheEvict(value = "unites", allEntries = true)
    public Unite create(Unite i) {
        return uniteDao.save(i);
    }

    @Override
    @Cacheable("unite")
    public Unite findById(int id) {
        return uniteDao.findById(id).orElse(null);
    }

    @Override
    @Cacheable("unite")
    public List<Unite> findByNom(String nom) {
        return uniteDao.findByNom(nom);
    }

    @Override
    @Cacheable("unites")
    public List<Unite> findAll() {
        return uniteDao.findAll(Sort.by(Sort.Direction.ASC, "order"));
    }


}
