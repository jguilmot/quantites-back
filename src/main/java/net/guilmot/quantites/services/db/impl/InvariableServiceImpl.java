package net.guilmot.quantites.services.db.impl;

import net.guilmot.quantites.dao.InvariableDao;
import net.guilmot.quantites.entities.db.Invariable;
import net.guilmot.quantites.services.db.InvariableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("invariableService")
public class InvariableServiceImpl implements InvariableService {

    @Autowired
    private InvariableDao invariableDao;

    @Override
    @CacheEvict(value = "invariables", allEntries = true)
    public Invariable create(Invariable i) {
        return invariableDao.save(i);
    }

    @Override
    @Cacheable(value = "invariable")
    public Invariable findById(int id) {
        return invariableDao.findById(id).orElse(null);
    }

    @Override
    @Cacheable(value = "invariable")
    public List<Invariable> findByNom(String nom) {
        return invariableDao.findByNom(nom);
    }

    @Override
    @Cacheable(value = "invariables")
    public List<Invariable> findAll() {
        return invariableDao.findAll(Sort.by(Sort.Direction.ASC, "nom"));
    }

    @Override
    @CacheEvict(value = "invariables", allEntries = true)
    public void deleteAll() {
        invariableDao.deleteAll();
    }

    @Override
    @CacheEvict(value = "invariable", key = "#i")
    public void delete(Invariable i) {
        invariableDao.delete(i);
    }

    @Override
    public List<Invariable> createOrUpdate(List<Invariable> i) {
        deleteAll();
        i.forEach(this::create);
        return i;
    }
}
