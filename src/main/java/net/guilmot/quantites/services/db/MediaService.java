package net.guilmot.quantites.services.db;

import net.guilmot.quantites.entities.db.Media;

import java.util.List;

public interface MediaService {

    Media create(Media m);

    Media findById(int id);

    List<Media> findAll();
}
