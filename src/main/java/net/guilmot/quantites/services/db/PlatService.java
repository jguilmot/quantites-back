package net.guilmot.quantites.services.db;

import net.guilmot.quantites.entities.db.Plat;

import java.util.List;

public interface PlatService {

    Plat create(Plat p);

    Plat findById(int id);

    List<Plat> findByNom(String nom);

    List<Plat> findAll();

}
