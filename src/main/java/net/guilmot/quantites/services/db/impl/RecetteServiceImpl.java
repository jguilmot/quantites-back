package net.guilmot.quantites.services.db.impl;

import lombok.extern.slf4j.Slf4j;
import net.guilmot.quantites.dao.RecetteDao;
import net.guilmot.quantites.entities.db.Plat;
import net.guilmot.quantites.entities.db.Recette;
import net.guilmot.quantites.services.db.PlatService;
import net.guilmot.quantites.services.db.RecetteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service("recetteService")
public class RecetteServiceImpl implements RecetteService {

    @Autowired
    private RecetteDao recetteDao;

    @Autowired
    private PlatService platService;


    @Override
    public Recette create(Recette r) {
        return recetteDao.save(r);
    }

    @Override
    public void delete(Integer idRecette) {
        recetteDao.findById(idRecette).ifPresent(recette -> recetteDao.delete(recette));
    }

    @Override
    public Recette findById(int id) {
        return recetteDao.findById(id).orElse(null);
    }

    @Override
    public List<Recette> findByNom(String nom) {
        return recetteDao.findByNom(nom, Sort.by(Sort.Direction.ASC, "nom"));
    }

    @Override
    public List<Recette> findByNomContainingIgnoreCase(String nom) {
        return recetteDao.findByNomContainingIgnoreCase(nom, Sort.by(Sort.Direction.ASC, "nom"));
    }

    @Override
    public List<Recette> findByPlatId(int idPlat) {
        return recetteDao.findByPlatId(idPlat);
    }

    @Override
    public List<Recette> findByPlat(Plat p) {
        return recetteDao.findByPlat(p);
    }

    @Override
    public List<Recette> findByPlatNom(String s) {

        List<Recette> recetteByPlatNom = new ArrayList<>();
        List<Recette> recetteByPlatId;

        List<Plat> plats = platService.findByNom(s);
        for (Plat plat : plats
        ) {
            recetteByPlatId = recetteDao.findByPlatId(plat.getId());
            recetteByPlatNom.addAll(recetteByPlatId);
        }
        return recetteByPlatNom;
    }





    @Override
    public List<Recette> findAll() {
        return recetteDao.findAll(Sort.by(Sort.Direction.ASC, "nom"));
    }
}
