package net.guilmot.quantites.services.metier;

import net.guilmot.quantites.entities.metier.Referentiel;
import net.guilmot.quantites.entities.metier.ReferentielMetier;
import net.guilmot.quantites.entities.metier.ReferentielParametrage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service("referentielService")
public class ReferentielService {

    @Autowired
    private ReferentielParametrageService referentielParametrageService;

    @Autowired
    private ReferentielMetierService referentielMetierService;

    public Referentiel getReferentielAll(boolean forceReloadParametrage, boolean forceReloadMetier) {

        if (Objects.isNull(ReferentielParametrageService.getREFERENTIEL_PARAMETRAGE().getInvariables()) || forceReloadParametrage) {
            referentielParametrageService.initReferentiel();
        }

        if (Objects.isNull(ReferentielMetierService.getREFERENTIEL_METIER().getRecettesIngredients()) || forceReloadMetier) {
            referentielMetierService.initReferentiel();
        }

        return new Referentiel(ReferentielParametrageService.getREFERENTIEL_PARAMETRAGE(), ReferentielMetierService.getREFERENTIEL_METIER());
    }

    public ReferentielParametrage getReferentielParametrage(boolean forceReload) {

        if (Objects.isNull(ReferentielParametrageService.getREFERENTIEL_PARAMETRAGE().getInvariables()) || forceReload) {
            referentielParametrageService.initReferentiel();
        }

        return ReferentielParametrageService.getREFERENTIEL_PARAMETRAGE();
    }

    public ReferentielMetier getReferentielMetier(boolean forceReload) {

        if (Objects.isNull(ReferentielMetierService.getREFERENTIEL_METIER().getRecettesIngredients()) || forceReload) {
            referentielMetierService.initReferentiel();
        }

        return ReferentielMetierService.getREFERENTIEL_METIER();
    }


}
