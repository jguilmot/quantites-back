package net.guilmot.quantites.services.metier;

import lombok.Getter;
import net.guilmot.quantites.entities.metier.ReferentielMetier;
import net.guilmot.quantites.services.db.DetailsRecetteService;
import net.guilmot.quantites.services.db.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("referentielMetierService")
public class ReferentielMetierService {

    @Getter
    private static final ReferentielMetier REFERENTIEL_METIER = new ReferentielMetier();

    @Autowired
    private DetailsRecetteService detailsRecetteService;

    @Autowired
    private IngredientService ingredientService;

    public void initReferentiel() {
        REFERENTIEL_METIER.setRecettesIngredients(detailsRecetteService.findRecettesIngredientsAll());
        REFERENTIEL_METIER.setIngredients(ingredientService.findAll());

    }

}
