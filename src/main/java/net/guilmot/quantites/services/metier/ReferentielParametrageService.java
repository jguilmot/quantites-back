package net.guilmot.quantites.services.metier;

import lombok.Getter;
import net.guilmot.quantites.entities.metier.ReferentielParametrage;
import net.guilmot.quantites.services.db.InvariableService;
import net.guilmot.quantites.services.db.UniteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("referentielParametrageService")
public class ReferentielParametrageService {

    @Getter
    private static final ReferentielParametrage REFERENTIEL_PARAMETRAGE = new ReferentielParametrage();
    @Autowired
    private InvariableService invariableService;
    @Autowired
    private UniteService uniteService;

    public void initReferentiel() {
        REFERENTIEL_PARAMETRAGE.setInvariables(invariableService.findAll());
        REFERENTIEL_PARAMETRAGE.setUnites(uniteService.findAll());
    }


}
