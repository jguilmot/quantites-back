package net.guilmot.quantites.services.metier.impl;


import net.guilmot.quantites.entities.db.DetailsRecette;
import net.guilmot.quantites.entities.db.Ingredient;
import net.guilmot.quantites.entities.db.Media;
import net.guilmot.quantites.entities.db.Plat;
import net.guilmot.quantites.entities.db.Unite;
import net.guilmot.quantites.entities.metier.DetailIngredient;
import net.guilmot.quantites.entities.metier.FormRecette;
import net.guilmot.quantites.services.db.DetailsRecetteService;
import net.guilmot.quantites.services.db.IngredientService;
import net.guilmot.quantites.services.db.MediaService;
import net.guilmot.quantites.services.db.PlatService;
import net.guilmot.quantites.services.db.RecetteService;
import net.guilmot.quantites.services.metier.FormRecetteService;
import net.guilmot.quantites.services.metier.ReferentielService;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;


@Service("formRecetteService")
public class FormRecetteServiceImpl implements FormRecetteService {

    @Autowired
    private DetailsRecetteService detailsRecetteService;

    @Autowired
    private MediaService mediaService;
    @Autowired
    private PlatService platService;

    @Autowired
    private RecetteService recetteService;

    @Autowired
    private IngredientService ingredientService;

    @Autowired
    private ReferentielService referentielService;


    @Override
    public void delete(Integer idRecette) {
        List<DetailsRecette> detailsRecettes = detailsRecetteService.findByRecetteId(idRecette);
        detailsRecettes.forEach(detailsRecette -> detailsRecetteService.delete(detailsRecette));
        recetteService.delete(idRecette);
    }

    private static void isValid(FormRecette f) {

        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(Objects.requireNonNull(FormRecetteServiceImpl.class.getResourceAsStream("/schemaFormRecette.json"))));
        JSONObject jsonSubject = new JSONObject(f);

        Schema schema = SchemaLoader.load(jsonSchema);
        schema.validate(jsonSubject);


    }

    @Override
    public FormRecette createOrUpdate(FormRecette f) {

        // Contrôle du formulaire FormRecette
        isValid(f);

        // Formatage du formulaire
        formatFormRecette(f);

        // Est-ce-que le plat existe déjà ?
        // Si oui, on récupère l'id, sinon on crée le plat
        Optional<Plat> answerPlats = platService.findByNom(f.getPlat().getNom()).stream().findFirst();
        if (answerPlats.isPresent()) {
            f.getPlat().setId(answerPlats.get().getId());
        } else {
            Plat plat = platService.create(f.getPlat());
            f.getPlat().setId(plat.getId());
        }
        f.getRecette().setPlat(f.getPlat());

        // Est-ce-que le media scan existe déjà (si id scan est différent de null et zéro) ?
        // Si non on initialise à null le scan
        if (f.getRecette().getScan().getId() != null && f.getRecette().getScan().getId() != 0) {
            Optional<Media> answerScanMedia = Optional.ofNullable(mediaService.findById(f.getRecette().getScan().getId()));
            if (answerScanMedia.isEmpty()) {
                f.getRecette().setScan(null);
            }
        } else {
            f.getRecette().setScan(null);
        }

        // Est-ce-que le media photo existe déjà (si id photo est différent de null et zéro) ?
        // Si non on initialise à null la photo
        if (f.getRecette().getPhoto().getId() != null && f.getRecette().getPhoto().getId() != 0) {
            Optional<Media> answerPhotoMedia = Optional.ofNullable(mediaService.findById(f.getRecette().getPhoto().getId()));
            if (answerPhotoMedia.isEmpty()) {
                f.getRecette().setPhoto(null);
            }
        } else {
            f.getRecette().setPhoto(null);
        }

        // Est-ce-que la recette existe déjà ?
        // Si oui on la supprime ainsi que tous les détailsIngrédients associés
        if (recetteService.findById(f.getRecette().getId()) != null) {
            delete(f.getRecette().getId());
        }
        // Qu'elle existe ou pas on crée la recette
        f.getRecette().setId(recetteService.create(f.getRecette()).getId());

        // 1) Est-ce-que les ingrédients de la liste detailIngredients existent déjà ?
        // Si oui on récupère les id, sinon on crée les ingrédients
        // 2) On récupère l'id des unités de la liste detailIngredients
        // 3) On crée les occurrences DetailsRecette
        f.getDetailIngredients().forEach(detailIngredient -> {

            Optional<Ingredient> answerIngredient = ingredientService.findByNom(detailIngredient.getIngredient().getNom()).stream().findFirst();
            if (answerIngredient.isPresent()) {
                detailIngredient.getIngredient().setId(answerIngredient.get().getId());
            } else {
                Ingredient ingredient = ingredientService.create(detailIngredient.getIngredient());
                detailIngredient.getIngredient().setId(ingredient.getId());
            }

            Optional<Unite> answerUnite = referentielService.getReferentielParametrage(false).getUnites().stream().filter(u -> u.getNom().equals(detailIngredient.getUnite().getNom())).findFirst();
            answerUnite.ifPresent(detailIngredient::setUnite);

            detailsRecetteService.create(new DetailsRecette(detailIngredient.getQuantite(), detailIngredient.getUnite(), f.getRecette(), detailIngredient.getIngredient()));

        });

        return f;
    }

    private void formatFormRecette(FormRecette f) {


        // -- FORMATAGE PLAT --
        // Retrait des espaces avant et après
        f.getPlat().setNom(f.getPlat().getNom().trim());
        // Capitalisation du premier caractère
        f.getPlat().setNom(f.getPlat().getNom().substring(0, 1).toUpperCase(Locale.FRENCH) + f.getPlat().getNom().substring(1));

        // - FORMATAGE RECETTE --
        // Retrait des espaces avant et après
        f.getRecette().setNom(f.getRecette().getNom().trim());
        // Capitalisation du premier caractère
        f.getRecette().setNom(f.getRecette().getNom().substring(0, 1).toUpperCase(Locale.FRENCH) + f.getRecette().getNom().substring(1));

        // - FORMATAGE DETAIL INGREDIENT --
        for (DetailIngredient detailIngredient : f.getDetailIngredients()) {
            // Ingredient

            detailIngredient.getIngredient().setNom(ingredientService.formatIngredient(detailIngredient.getIngredient().getNom()));

            // Unité
            // Mise en minuscule
            detailIngredient.getUnite().setNom(detailIngredient.getUnite().getNom().trim().toLowerCase(Locale.FRENCH));
        }

    }


}
