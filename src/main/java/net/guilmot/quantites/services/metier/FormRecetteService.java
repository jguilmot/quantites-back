package net.guilmot.quantites.services.metier;

import net.guilmot.quantites.entities.metier.FormRecette;


public interface FormRecetteService {
    FormRecette createOrUpdate(FormRecette formRecette);
    void delete(Integer idRecette);
}
