package net.guilmot.quantites.exceptions;


import org.everit.json.schema.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class GlobalControllerExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<Erreur> onIllegalArgumentException(IllegalArgumentException exception) {
        return new ResponseEntity(new Erreur(HttpStatus.BAD_REQUEST.toString(), exception.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<Erreur> onHttpMessageNotReadableException(HttpMessageNotReadableException exception) {
        return new ResponseEntity(new Erreur(HttpStatus.BAD_REQUEST.toString(), exception.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<Erreur> onValidationException(ValidationException exception) {
        return new ResponseEntity(new Erreur(HttpStatus.BAD_REQUEST.toString(), exception.getMessage()), HttpStatus.BAD_REQUEST);
    }


}
