package net.guilmot.quantites.exceptions;

import lombok.Getter;

@Getter
public enum ControllerMessage {

    BAD_REQUEST_FORM_RECETTE("Paramètres incorrects empêchant la création de la recette");
    private final String message;

    ControllerMessage(final String message){
        this.message = message;
    }
}
