package net.guilmot.quantites.dao;

import net.guilmot.quantites.entities.db.Plat;
import net.guilmot.quantites.entities.db.Recette;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RecetteDao extends JpaRepository<Recette, Integer> {
    Optional<Recette> findById(Integer id);

    List<Recette> findByNomContainingIgnoreCase(String nom, Sort sort);

    List<Recette> findByNom(String nom, Sort sort);

    List<Recette> findByPlat(Plat plat);

    List<Recette> findByPlatId(Integer id);
}
