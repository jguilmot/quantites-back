package net.guilmot.quantites.dao;


import net.guilmot.quantites.entities.db.Invariable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InvariableDao extends JpaRepository<Invariable, Integer> {
    Optional<Invariable> findById(Integer id);

    List<Invariable> findByNom(String nom);
}
