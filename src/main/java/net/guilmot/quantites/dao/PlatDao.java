package net.guilmot.quantites.dao;

import net.guilmot.quantites.entities.db.Plat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PlatDao extends JpaRepository<Plat, Integer> {

    Optional<Plat> findById(Integer id);

    List<Plat> findByNom(String nom);
}
