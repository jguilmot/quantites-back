package net.guilmot.quantites.dao;

import net.guilmot.quantites.entities.db.DetailsRecette;
import net.guilmot.quantites.entities.db.Ingredient;
import net.guilmot.quantites.entities.db.Recette;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DetailsRecetteDao extends JpaRepository<DetailsRecette, Integer> {
    Optional<DetailsRecette> findById(Integer id);

    List<DetailsRecette> findByRecette(Recette recette);

    List<DetailsRecette> findByRecetteId(Integer id);

    List<DetailsRecette> findByIngredient(Ingredient ingredient);

    List<DetailsRecette> findByIngredientId(Integer id);
}
