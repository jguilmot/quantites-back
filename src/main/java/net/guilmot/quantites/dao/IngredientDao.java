package net.guilmot.quantites.dao;

import net.guilmot.quantites.entities.db.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IngredientDao extends JpaRepository<Ingredient, Integer> {
    Optional<Ingredient> findById(Integer id);
    List<Ingredient> findByNom(String nom);
    List<Ingredient> findByNomContainingIgnoreCase(String nom);
}
