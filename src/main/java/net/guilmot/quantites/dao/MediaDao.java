package net.guilmot.quantites.dao;

import net.guilmot.quantites.entities.db.Media;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MediaDao extends JpaRepository<Media, Integer> {
    Optional<Media> findById(Integer id);
}
