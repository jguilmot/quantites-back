package net.guilmot.quantites.dao;

import net.guilmot.quantites.entities.db.Unite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UniteDao extends JpaRepository<Unite, Integer> {
    Optional<Unite> findById(Integer id);

    List<Unite> findByNom(String nom);

}
